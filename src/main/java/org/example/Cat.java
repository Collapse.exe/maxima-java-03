package org.example;

public class Cat {
    private String name;
    private int weight;
    private boolean isAngry;

    public Cat(String name, int weight, boolean isAngry) throws IncorrectWeightException {
        this.name = name;
        this.weight = weight;
        this.isAngry = isAngry;
        if (weight < 0){
            throw new IncorrectWeightException("Некорректный вес");
        }
    }

    @Override
    public String toString() {
        return String.format("Cat{name='%s', weight=%d, isAngry=%b}", name,weight,isAngry);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) throws IncorrectWeightException {
        if (weight < 0)
            throw new IncorrectWeightException("Некорректный вес");
        else this.weight = weight;
    }

    public boolean isAngry() {
        return isAngry;
    }

    public void setAngry(boolean angry) {
        isAngry = angry;
    }
}
