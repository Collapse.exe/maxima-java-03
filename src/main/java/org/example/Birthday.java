package org.example;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Birthday {
    public static LocalDate nextBirthday (int year, int month, int day){
        int diff = 1000 - getAge(year,month,day) % 1000;
        return LocalDate.now().plusDays(diff);
    }
    public static int getAge (int year, int month, int day){
        return  (int) ChronoUnit.DAYS.between(LocalDate.of(year, month, day),LocalDate.now());
    }
}
