package org.example;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        Dog tuzik = new Dog("Tuzik", 10, false);
        Dog sharik = new Dog("Sharik", 8, false);
        Cat ramzes = new Cat("Ramzes", 6, true);
        Cat tosha = new Cat("Tosha", 5, false);
        Cat boba = new Cat("Боба", 11, false);
        Cat sanya = new Cat("Саня", 4, true);
        Cat ramzy = new Cat("ramzy", 7, true);

        ArrayList<Cat> cts = new ArrayList<>();
        cts.add(ramzes);
        cts.add(boba);
        cts.add(sanya);
        cts.add(tosha);
        cts.add(sanya);
        cts.add(ramzy);
        //System.out.println(CatStatistics.groupCatsByFirstLetter(cts));
        //System.out.println(CatStatistics.getCommonWeight(cts, false));
        StackKitchen<Cat> sk = new StackKitchen<>(cts);
        System.out.println(sk.getAnimals());
        sk.add(tosha);
        sk.add(boba);
        System.out.println(sk.getAnimals());
        sk.feed();
        System.out.println(sk.getAnimals());
        QueueKitchen<Cat> queueKitchen = new QueueKitchen<>();
        System.out.println(queueKitchen.getAnimals());
        queueKitchen.add(sanya);
        queueKitchen.add(ramzes);
        System.out.println(queueKitchen.getAnimals());
        queueKitchen.feed();
        System.out.println(queueKitchen.getAnimals());
        StreamTransformer streamTransformer = new StreamTransformer();
        streamTransformer.transform("CatInfo.csv", "Какие есть коты.txt");

    }
}
