package org.example;

import java.io.IOException;

public interface Transformable {
    void transform(String fileIN, String fileOut) throws IOException;
}
