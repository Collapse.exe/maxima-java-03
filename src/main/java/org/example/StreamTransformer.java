package org.example;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class StreamTransformer implements Transformable {

    @Override
    public void transform(String fileIN, String fileOut) throws IOException {
        FileInputStream stream = new FileInputStream(fileIN);
        BufferedInputStream bufStream = new BufferedInputStream(stream);
        StringBuilder result = new StringBuilder();

        byte[] byteProperties = bufStream.readAllBytes();
        bufStream.close();

        String allProperties = new String(byteProperties, StandardCharsets.UTF_8);
        String [] properties = allProperties.split(System.getProperty("line.separator"));

        for (String catProperty: properties) {
            properties = catProperty.split(";");
            if (properties.length != 3) {
                result.append("Некорректные данные кота.\n");
            } else {
                String isAngry = "Дружелюбный";
                if (properties[2].trim().equals("true")) {
                    isAngry = "Сердитый";
                }
                result.append(String.format("%s кот %s весом %s кг. %n", isAngry,properties[0],properties[1]));
            }
        }
        FileOutputStream outStream = new FileOutputStream(fileOut);
        outStream.write(result.toString().getBytes(StandardCharsets.UTF_8));
        outStream.flush();
        outStream.close();
    }
}
