package org.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class TextTransformer implements Transformable {
    public void transform(String fileIN, String fileOut) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileIN));
        FileWriter fw = new FileWriter(fileOut, false);
        String catProperties;
        StringBuilder sb = new StringBuilder();
        while ((catProperties = br.readLine()) != null) {
            String[] catsProperties = catProperties.split(";");
            String isAngry = "Дружелюбный";
            if (catsProperties[2].equals("true")) {
                isAngry = "Сердитый";
            }
            sb.append(isAngry)
                    .append(" кот ")
                    .append(catsProperties[0])
                    .append(" весом ")
                    .append(catsProperties[1])
                    .append(" кг.")
                    .append('\n');
        }
        br.close();
        fw.write(String.valueOf(sb));
        fw.close();
    }
}