package org.example;

public class CatFactory{
    public static Cat createCat(String name, int weight) throws IncorrectWeightException {
        Cat cat;
        try {
            cat = new Cat(name, weight, false);
        }
        catch (IncorrectWeightException we){
            cat = new Cat(name, 5, true);
        }

        return cat;
    }
}
