package org.example;

import java.util.ArrayList;

public class StackKitchen<T> implements AnimalKitchen<T> {
    private ArrayList<T> animals = new ArrayList<>();

    public StackKitchen() {
    }

    public StackKitchen(ArrayList<T> animals) {
        this.animals = animals;
    }

    public ArrayList<T> getAnimals() {
        return animals;
    }
    @Override
    public void add (T animal) {
        this.animals.add(animal);
    }

    @Override
    public void feed() {
        animals.remove(animals.size() - 1);
    }
}
