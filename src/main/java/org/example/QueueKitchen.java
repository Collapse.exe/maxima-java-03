package org.example;

import java.util.ArrayList;

public class QueueKitchen<T> implements AnimalKitchen<T> {
    private ArrayList<T> animals = new ArrayList<>();

    public QueueKitchen() {
    }

    public QueueKitchen(ArrayList<T> animals) {
        this.animals = animals;
    }

    public ArrayList<T> getAnimals() {
        return animals;
    }

    @Override
    public void add (T animal) {
        this.animals.add(animal);
    }

    @Override
    public void feed() {
        animals.remove(0);
    }
}