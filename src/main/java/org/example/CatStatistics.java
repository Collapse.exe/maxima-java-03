package org.example;

import java.util.*;
import java.util.stream.Collectors;

public class CatStatistics {
    public static ArrayList<Cat> sortByNameAscending(ArrayList<Cat> cats){
        ArrayList<Cat> sortedList;
        sortedList = cats.stream()
                .sorted(Comparator.comparing(Cat::getName))
                .collect(Collectors.toCollection(ArrayList::new));
        //сats.sort(((o1, o2) -> o1.getName().compareTo(o2.getName()))); // для перезаписи исходной коллекции
        return sortedList;
    }
    public static ArrayList<Cat> sortByWeightDescending(ArrayList<Cat> cats){
        ArrayList<Cat> sortedList;
        sortedList = cats.stream()
                .sorted((cat1, cat2) -> cat2.getWeight() - cat1.getWeight())
                .collect(Collectors.toCollection(ArrayList::new));
        //cats.sort(((o1, o2) -> o2.getWeight() - o1.getWeight())); // для перезаписи исходной коллекции
        return sortedList;
    }
    public static ArrayList<Cat> removeFirstAndLast (ArrayList<Cat> cats){
        ArrayList<Cat> result;
        result = cats.stream()
                .skip(1)
                .limit(cats.size() - 2)
                .collect(Collectors.toCollection(ArrayList::new));
        /*cats.remove(0);
        cats.remove(cats.size() - 1);*/ // для перезаписи исходной коллекции
        return result;
    }
    public static Cat findFirstNonAngryCat (ArrayList<Cat> cats) throws Exception {
        return cats.stream()
                .filter(cat1 -> !cat1.isAngry())
                .findFirst()
                .orElseThrow(Exception::new);
    }
    public static int getCommonWeight(ArrayList<Cat> cats, boolean onlyAngry){
        return cats.stream().filter(cat -> !onlyAngry || cat.isAngry())
                .map(Cat::getWeight)
                .reduce(Integer::sum)
                .orElse(0);
    }
    public static Map<String, List<Cat>> groupCatsByFirstLetter (ArrayList<Cat> cats){
        return cats.stream()
                .collect(Collectors.groupingBy(cat -> cat.getName().substring(0,1).toUpperCase()))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }
}

