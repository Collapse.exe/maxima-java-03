package org.example;

public class IncorrectWeightException extends Exception {
    public IncorrectWeightException(String message) {
        super(message);
    }
}
