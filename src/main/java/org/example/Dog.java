package org.example;

public class Dog {
    private String name;
    private int weight;
    private boolean isAngry;


    public Dog(String name, int weight, boolean isAngry) throws IncorrectWeightException {
        this.name = name;
        this.weight = weight;
        this.isAngry = isAngry;
        if (weight < 0){
            throw new IncorrectWeightException("Некорректный вес");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) throws IncorrectWeightException {
        if (weight < 0)
            throw new IncorrectWeightException("Некорректный вес");
        else this.weight = weight;
    }

    public boolean isAngry() {
        return isAngry;
    }

    public void setAngry(boolean angry) {
        isAngry = angry;
    }
}
